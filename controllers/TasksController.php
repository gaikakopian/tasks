<?php
class TasksController {
    public function index() {
        $tasks = Tasks::all();
        require_once('views/tasks/index.php');
    }
    public function create(){
        require_once('views/tasks/create.php');
    }
    public function save(){
        $data = $_POST;
        $create = Tasks::create($data);
        if($create['success']){
            self::index();
        }else{
            self::create();
        }
    }
    public function edit(){

        if (!isset($_GET['id']))
            return call('pages', 'error');

        $task = Tasks::find($_GET['id']);
        $checked = '';
        if($task->to_do == '1'){
            $checked = 'checked';
        }
        require_once('views/tasks/edit.php');
    }
    public function update(){
        $data = $_POST;
        $update = Tasks::update($data);
        self::index();
    }
}
?>