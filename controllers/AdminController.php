<?php
class AdminController {
    public function login() {
        if(isset($_SESSION["is_admin"])){
            $tasks = Tasks::all();
            require_once('views/tasks/index.php');
        }else{
            require_once('views/admin/login.php');
        }
    }
    public function sign(){
        if($_POST['username'] == 'admin' && $_POST['password'] == '123'){
            $_SESSION["is_admin"] = 1;
        }
        self::login();
    }
    public function logout(){
        if(isset($_SESSION["is_admin"])){
            unset($_SESSION["is_admin"]);
            self::login();
        }
    }
}
?>