<?php
class Tasks {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $user_name;
    public $email;
    public $task_content;
    public $task_photo;
    public $to_do;
    public $reg_date;

    public function __construct($id, $user_name, $email,$task_content,$task_photo,$to_do,$reg_date) {
        $this->id      = $id;
        $this->user_name  = $user_name;
        $this->email  = $email;
        $this->task_content = $task_content;
        $this->task_photo = $task_photo;
        $this->to_do = $to_do;
        $this->reg_date = $reg_date;
    }

    public static function all() {

        $db = Db::getInstance();
        $total = $db ->query('
            SELECT
                COUNT(*)
            FROM
                tasks
        ')->fetchColumn();

        // How many items to list per page
        $limit = 3;
        // How many pages will there be
        $pages = ceil($total / $limit);

        // What page are we currently on?
        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
            'options' => array(
                'default'   => 1,
                'min_range' => 1,
            ),
        )));

        // Calculate the offset for the query
        $offset = ($page - 1)  * $limit;

        // Some information to display to the user
        $start = $offset + 1;
        $end = min(($offset + $limit), $total);

        // The "back" link
        $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

        // The "forward" link
        $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';


        $pagination_html = '<div id="paging"><p>'. $prevlink. ' Page '. $page. ' of '. $pages. ' pages, displaying '. $start. '-'. $end. ' of '. $total. ' results '. $nextlink. ' </p></div>';

        if (isset($_GET['sort'])){
            $sort = $_GET['sort'];
        } else {
            $sort = 'user_name';
        }
        // Prepare the paged query
        $stmt = $db->prepare('
            SELECT
                *
            FROM
                tasks
            ORDER BY
                '.$sort.'
            LIMIT
                :limit
            OFFSET
                :offset
        ');

        // Bind the query params
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $offset, PDO::PARAM_INT);
        $stmt->execute();

        // Define how we want to fetch the results
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $iterator = new IteratorIterator($stmt);

        // Display the results
        foreach ($iterator as $task) {
            $list[] = new Tasks($task['id'], $task['user_name'], $task['email'],$task['task_content'],$task['task_photo'],$task['to_do'],$task['reg_date']);
        }

        return array('list' => $list , 'pagination' => $pagination_html);

    }

    public static function find($id) {
        $db = Db::getInstance();
        // we make sure $id is an integer
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM tasks WHERE id = :id');
        // the query was prepared, now we replace :id with our actual $id value
        $req->execute(array('id' => $id));
        $post = $req->fetch();

        return new Tasks($post['id'], $post['user_name'], $post['email'],$post['task_content'],$post['task_photo'],$post['to_do'],$post['reg_date']);
    }

    public function create($data){
        if(!empty($data['user_name']) && !empty($data['email']) && !empty($data['task_content'])){
            $upload = Tasks::checkImage();
            if($upload['success']) {
                $data['task_photo'] = $upload['name'];

                $db = Db::getInstance();

                $query = "INSERT INTO tasks (user_name, email, task_content, task_photo, to_do) VALUES ('".$data['user_name']."','".$data['email']."','".$data['task_content']."','".$data['task_photo']."','0')";
                $req = $db->query($query);
                return array('success' => true);
            }else{
                return array('success' => false, 'message' => $upload['message']);
            }
        }else{
            return array('success' => false, 'message' => 'Not valid information');
        }
    }

    public function update($data){
        if(empty($data['to_do'])){
            $data['to_do'] = 0;
        }

        $db = Db::getInstance();
        $query = "UPDATE tasks SET task_content = '". $data['task_content'] ."', to_do = '". $data['to_do'] ."' WHERE id = ". $data['id'] ."";
        $req = $db->query($query);
    }

    public function checkImage(){
        if(!empty($_FILES['task_photo']) && !empty($_FILES['task_photo']['name'])){

            $uploading_file = getimagesize($_FILES['task_photo']["tmp_name"]);
            if($uploading_file['mime'] == 'image/jpeg' || $uploading_file['mime'] == 'image/gif' || $uploading_file['mime'] == 'image/png' || $uploading_file['mime'] == 'image/jpg'){

                if($uploading_file['0'] > 320 || $uploading_file['1'] > 240){
                    self::resizeImage($_FILES['task_photo']['tmp_name'] , 320 , 240);
                }

                $path_parts = pathinfo($_FILES["task_photo"]["name"]);
                $name = time() .'.'. $path_parts['extension'];
                $uploadfile = dirname(__DIR__). '/public/images/' . $name;

                if (move_uploaded_file($_FILES['task_photo']['tmp_name'],$uploadfile)) {
                    return array('success' => true, 'name' => $name);
                } else {
                    return array('success' => false, 'message' => 'There was some problem when uploading file');
                }
            }else{
                return array('success' => false, 'message' => 'This file type is not supported');
            }
        }
    }

    public function resizeImage($file, $w, $h) {

        list($width, $height, $type, $attr) = getimagesize( $file );
        $target_filename = $file;
        $src = imagecreatefromstring( file_get_contents( $file ) );
        $dst = imagecreatetruecolor( 320, 240 );
        imagecopyresampled( $dst, $src, 0, 0, 0, 0, $w, $h, $width, $height );
        imagedestroy( $src );
        imagepng( $dst, $target_filename ); // adjust format as needed
        imagedestroy( $dst );
    }
}
?>