<?php
  function call($controller, $action) {

    require_once('controllers/' . ucfirst ($controller) . 'Controller.php');

    switch($controller) {

    case 'pages':
        $controller = new PagesController();
    break;
    case 'tasks':
        require_once('models/Tasks.php');
        $controller = new TasksController();
        break;
    case 'admin':
        require_once('models/Tasks.php');
        $controller = new AdminController();
        break;
    }

    $controller->{ $action }();
    }

        // we're adding an entry for the new controller and its actions
        $controllers = array('pages' => ['home', 'error'],
                       'posts' => ['index', 'show'],
                       'tasks' => ['index','create','save','edit','update'],
                       'admin' => ['login','sign','logout']
         );

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>