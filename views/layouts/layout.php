<DOCTYPE html>
<html>
  <head>
      <!-- Latest compiled and minified CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

      <!-- jQuery library -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <header>
      <div class="container">
          <a href='?controller=tasks&action=create' class="btn btn-primary">Add Task</a>
          <a class="btn btn-primary" href='/'>Home</a>
          <?php session_start(); ?>
          <?php
              if(isset($_SESSION["is_admin"])){
                  $url = 'logout';
                  $title = 'Logout';
              }else{
                  $url = 'login';
                  $title = 'Login';
              }
          ?>
          <a class="btn btn-primary" href='?controller=admin&action=<?php echo $url ?>'><?php echo $title ?></a>
      </div>
    </header>

        <div class="container">
            <div class="row">
                <?php require_once('routes.php'); ?>
            </div>
        </div>

    <footer></footer>
  <body>
<html>