<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>Id</th>
            <th><a href="?sort=user_name">User Name</a></th>
            <th><a href="?sort=email">Email</a></th>
            <th><a href="?sort=task_content">Content</a></th>
            <th>Photo</th>
            <th>To Do</th>
            <th>Creation Date</th>
            <?php if(isset($_SESSION["is_admin"])){ ?>
                <th>Edit</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($tasks['list'] as $task) { ?>
                <tr>
                    <td><?php echo $task->id; ?></td>
                    <td><?php echo $task->user_name; ?></td>
                    <td><?php echo $task->email; ?></td>
                    <td><?php echo $task->task_content; ?></td>
                    <td><img src="/public/images/<?= $task->task_photo ?>" height="320" width="240"></td>
                    <?php $to_do = $task->to_do == '1' ? 'Yes' : 'No' ?>
                    <td><?php echo $to_do; ?></td>
                    <td><?php echo $task->reg_date; ?></td>
                    <?php if(isset($_SESSION["is_admin"])){ ?>
                        <td><a href="?controller=tasks&action=edit&id=<?php echo $task->id; ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?= $tasks['pagination'] ?>
</div>


