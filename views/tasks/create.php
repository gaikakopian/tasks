<form action="?controller=tasks&action=save" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="exampleFormControlInput1">User Name</label>
        <input name="user_name" required="required" type="text" class="form-control" id="user_name" placeholder="Username">
    </div>
    <div class="form-group">
        <label for="exampleFormControlInput1">Email address</label>
        <input name="email" type="email" class="form-control" id="email" placeholder="name@example.com">
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Task Content</label>
        <textarea name="task_content" class="form-control" id="task_content" rows="3"></textarea>
    </div>
    <div class="form-group">
        <label for="exampleFormControlFile1">Photo</label>
        <input name="task_photo" type="file" class="form-control-file" id="exampleFormControlFile1">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>

    <span onclick="showPreview()"  class="btn btn-danger">Preview</span>
</form>

<div class="row" id="previeBlock" style="display:none">
    <table class="table">
        <thead>
            <tr>
                <th>User Name</th>
                <th>Email</th>
                <th>Content</th>
            </tr>
        </thead>
        <tbody id="content">

        </tbody>
    </table>
</div>

<script>
    function showPreview() {
        document.getElementById("previeBlock").style.display = 'block';
        var el = document.getElementById("content");
        var name = document.getElementById("user_name").value;
        var email = document.getElementById("email").value;
        var task_content = document.getElementById("task_content").value;

        document.getElementById("content").innerHTML = "<tr><td>"+ name +"</td><td>"+ email +"</td><td>"+ task_content +"</td></tr>";
    }
</script>