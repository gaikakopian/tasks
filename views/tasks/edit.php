<form action="?controller=tasks&action=update" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $task->id ?>">
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Task Content</label>
        <textarea name="task_content" class="form-control" id="exampleFormControlTextarea1" rows="3"><?php echo $task->task_content ?></textarea>
    </div>
    <div class="form-check">
        <input name="to_do" type="checkbox" class="form-check-input" id="exampleCheck1" name="to_do" value="1"<?php echo $checked ?>>
        <label class="form-check-label" for="exampleCheck1">To do</label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>